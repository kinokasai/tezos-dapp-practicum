# Exercise

## Ligo

### Compile the contract

```sh
make
```

### Originate the contract

```sh
make originate
```

## Web app

```sh
yarn watch
```

View the app in the browser at localhost:1234.