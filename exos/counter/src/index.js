"use strict";

import {Tezos} from '@taquito/taquito';
import {TezBridgeSigner} from '@taquito/tezbridge-signer';

// FIXME: Put your originated address here
let contract_address = "KT1MGDoCkk2L6zCfViRa8gzhFbxC3R877bUm";
var tk = Tezos;
// We query the sandbox node here
tk.setProvider({rpc: 'http://localhost:18731', signer: new TezBridgeSigner ()})

function render(elt) {
  document.querySelector('#info').innerHTML = elt
}

function update_storage() {
  tk.contract.at(contract_address)
  .then(contract => contract.storage())
  .then(storage => document.querySelector('#value').innerHTML = storage.toString())
}

function call_contract() {
  tk.contract.at(contract_address)
  .then(contract => {
    render("Waiting for signature...")
    return contract.methods.main(null).send();})
  .then(op => {
    render("Sent! Waiting for confirmation...");
    return op.confirmation();})
  .then(block => {
    render("confirmed!")
    return update_storage(); })
  .catch(err => {
    console.log(err)
    render(err.message)
  })
  return null;
}

update_storage();
document.querySelector('#button').addEventListener('click', call_contract);